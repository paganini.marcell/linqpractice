﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace linqProject
{
    class Program
    {
        static void Main(string[] args)
        {
            List<HorrorMovieKiller> killers = KillerList.GetKillersList();

            IEnumerable<HorrorMovieKiller> ikillers = KillerList.GetKillersList();

            //Order by rate (From highest to lowest)
            //killers = killers.OrderByDescending(x => x.StarRate).ToList();

            //Order by rate (From highest to lowest), then by name
            //killers = killers.OrderByDescending(x => x.StarRate).ThenBy(x => x.Name).ToList();

            //Get results with non-fictional stories
            //killers = killers.Where(x => x.FictionalStory == false).ToList();

            //Get results from the 90s
            //killers = killers.Where(x => x.FirstAppearanceYear >= 1990 && x.FirstAppearanceYear < 2000).ToList();

            //Get the sum of all stars
            //int starSum = killers.Sum(x => x.StarRate);
            //Console.WriteLine($"The sum of all stars is {starSum}");

            //Get the sum of all the stars from movies between 1970 and 1989
            //int starSum = killers.Where(x => x.FirstAppearanceYear >= 1970 && x.FirstAppearanceYear < 1990).Sum(x => x.StarRate);
            //Console.WriteLine($"The sum of all stars is {starSum}");

            //Get the minimum value
            //int minYear = killers.Min(x => x.FirstAppearanceYear);
            //Console.WriteLine($"The earliest killer appearance was {minYear}");

            //Get the minimum value with a condition
            //int minYear = killers.Where(x => x.FirstAppearanceYear > 1970).Min(x => x.FirstAppearanceYear);
            //Console.WriteLine($"The earliest killer appearance was {minYear}");

            //NEXT: USING Aggregate() FUNCTION

            //Using sql-like queries. Range variable 
            //ikillers = from killer in killers
            //          where killer.FictionalStory == false
            //          select killer;    //use ikillers in the foreach loop



            foreach (HorrorMovieKiller k in killers)
            {
                Console.WriteLine($"Name: {k.Name} | Main movie: {k.MainMovie} | Rate: {k.StarRate} | First Appearance: {k.FirstAppearanceYear} | " +
                    $"Fictional Story: {k.FictionalStory}" + Environment.NewLine);
            }
            
            Console.ReadLine();
        }
    }
}
