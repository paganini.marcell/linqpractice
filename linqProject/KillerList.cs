﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqProject
{
    public static class KillerList
    {
        public static List<HorrorMovieKiller> GetKillersList()
        {
            List<HorrorMovieKiller> killers = new List<HorrorMovieKiller>();

            killers.Add(new HorrorMovieKiller { Name = "Michael Myers", MainMovie = "Halloween", StarRate = 3, FirstAppearanceYear = 1978, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Freddy Krueger", MainMovie = "A nightmare on Elm Street", StarRate = 4, FirstAppearanceYear = 1984, FictionalStory = false });
            killers.Add(new HorrorMovieKiller { Name = "Jason Voorhees", MainMovie = "Friday the 13th", StarRate = 4, FirstAppearanceYear = 1980, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "It", MainMovie = "It", StarRate = 2, FirstAppearanceYear = 1986, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Hannibal Lecter", MainMovie = "Silence of the Lambs", StarRate = 5, FirstAppearanceYear = 1981, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Ghostface", MainMovie = "Scream", StarRate = 3, FirstAppearanceYear = 1996, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "PinHead", MainMovie = "Hellraiser", StarRate = 2, FirstAppearanceYear = 1986, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Norman Bates", MainMovie = "Psycho", StarRate = 4, FirstAppearanceYear = 1960, FictionalStory = false });
            killers.Add(new HorrorMovieKiller { Name = "Jack Torrance", MainMovie = "The Shining", StarRate = 5, FirstAppearanceYear = 1977, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Regan MacNeil", MainMovie = "The Exorcist", StarRate = 5, FirstAppearanceYear = 1971, FictionalStory = false });
            killers.Add(new HorrorMovieKiller { Name = "Annie Wilkes", MainMovie = "Misery", StarRate = 4, FirstAppearanceYear = 1987, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Carrie White", MainMovie = "Carrie", StarRate = 3, FirstAppearanceYear = 1974, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Leatherface", MainMovie = "The Texas Chainsaw Massacre", StarRate = 4, FirstAppearanceYear = 1974, FictionalStory = false });
            killers.Add(new HorrorMovieKiller { Name = "Alien", MainMovie = "Alien", StarRate = 4, FirstAppearanceYear = 1979, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Frankensteins's monster", MainMovie = "Frankenstein", StarRate = 3, FirstAppearanceYear = 1818, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Chucky", MainMovie = "Child's Play", StarRate = 5, FirstAppearanceYear = 1988, FictionalStory = false });
            killers.Add(new HorrorMovieKiller { Name = "The Jigsaw Killer", MainMovie = "Saw", StarRate = 4, FirstAppearanceYear = 2004, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Sadako Yamamura", MainMovie = "Ring", StarRate = 3, FirstAppearanceYear = 1991, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Damien Thorn", MainMovie = "The Omen", StarRate = 3, FirstAppearanceYear = 1976, FictionalStory = true });
            killers.Add(new HorrorMovieKiller { Name = "Count Orlok", MainMovie = "Nosferatu", StarRate = 5, FirstAppearanceYear = 1922, FictionalStory = true });

            return killers;
        }
    }
}
