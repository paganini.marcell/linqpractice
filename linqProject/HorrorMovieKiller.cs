﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linqProject
{
    public class HorrorMovieKiller
    {
        public string Name { get; set; }
        public string MainMovie { get; set; }
        public int StarRate { get; set; }
        public int FirstAppearanceYear { get; set; }
        public bool FictionalStory { get; set; }
    }
}
